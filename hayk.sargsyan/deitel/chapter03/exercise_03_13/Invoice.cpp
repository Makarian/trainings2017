#include <iostream>
#include <string>
#include "Invoice.hpp"

Invoice::Invoice(std::string number, std::string about, int quantity, int price)
{
    setNumber(number);
    setAbout(about);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setNumber(std::string number)
{
    number_ = number;
}

std::string
Invoice::getNumber()
{
    return number_;
}

void
Invoice::setAbout(std::string about)
{
    about_ = about;
}

std::string
Invoice::getAbout()
{
    return about_;
}

void 
Invoice::setQuantity(int quantity)
{
    if (quantity < 0) {
        quantity_ = 0;
        std::cout << "Info 1: Quantity is negative, it is setted to 0." << std::endl;
        return;
    }
    quantity_ = quantity;
}

int 
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPrice(int price)
{
    if (price < 0) {
        price_ = 0;
        std::cout << "Info 2: Price is negative, it is setted to 0." << std::endl;
        return;
    }
    price_ = price;
}

int
Invoice::getPrice()
{
    return price_;
}

int
Invoice::getInvoiceAmount()
{
    return getQuantity() * getPrice();
}

