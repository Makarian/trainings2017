#include <iostream>

int
main()
{
    int binaryNumber;
    
    std::cout << "Please enter a binary number: ";
    std::cin >> binaryNumber;

    int decimalNumber = 0;
    int pow = 1;

    while (binaryNumber != 0) {        
        decimalNumber += (binaryNumber % 10) * pow; 
        binaryNumber /= 10;
        pow *= 2;
    }

    std::cout << "Converted decimal number: "<< decimalNumber << std::endl;

    return 0;
}
