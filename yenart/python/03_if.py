import sys;

if len(sys.argv) < 2:
    print "Error 1: x should be given in args (e.g. python 03_if.py 7)";

x = int(sys.argv[1]);
if x < 0:
    x = 0;
    print "Negative zeroed";
elif x == 0:
    print "Zero";
else:
    print "Positive";

