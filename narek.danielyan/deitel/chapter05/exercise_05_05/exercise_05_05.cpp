#include<iostream>

int
main()
{
    int total = 0, count;
    std::cout << "Enter count: ";
    std::cin >> count;
    
    for (int counter = 0; counter < count; ++counter) {
        int number; 
        std::cout << "Enter number: ";
        std::cin >> number;
        total += number;
    }
    std::cout << "Total is " << total << std::endl;
    return 0;
}
