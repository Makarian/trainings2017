#include<iostream>

int
main() 
{
    int number1, number2;

    std::cin >> number1 >> number2;

    if (0 == number2) {
        std::cout << "Error 1: Second number can not be 0" << std::endl;
        return 1;
    }
    if (number1 % number2 == 0) {
        std::cout << "First number is a multiple of the second" << std::endl;
        return 0;
    }
    std::cout << "First number is not a multiple of the second" << std::endl;
    return 0;
}

