#include <iostream>

int
main()
{
    int number = 0;

    std::cout << "Input the number" << std::endl;
    std::cin >> number; 

    if (number % 2 == 0) {
        std::cout << "Number is even" << std::endl;
        return 0;
    }
    std::cout << "Number is odd " << std::endl;
    return 0;
}
