#include <iostream>

int 
main() 
{
    int number1, number2, number3, number4, number5;

    std::cout << "Enter five integers" << std::endl;
    std::cin >> number1;
    std::cin >> number2;
    std::cin >> number3;
    std::cin >> number4;
    std::cin >> number5;

    int biggest = number1, smallest = number1;

    if (number2 >= number1) {
        biggest = number2;
    } 
    if (number2 < number1) {
        smallest = number2;
    }
    if (number3 >= number2) {
        biggest = number3;
    }
    if (number3 < number2) {
        smallest = number3;
    }
    if (number4 >= number3) {
        biggest = number4;
    } 
    if (number4 < number3) {
        smallest = number4;
    }
    if (number5 >= number4) {
        biggest = number5;        
    }
    if (number5 < number4) {
        smallest = number5;
    }
    std::cout << "The biggest number is: " << biggest << "\n" << "The smallest number is: " << smallest << std::endl;
    return 0;
}

