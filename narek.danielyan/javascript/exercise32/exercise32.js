$(document).ready(function () {
    "use strict";
    var arr = ["apple", "orange"];
    arr.push("kiwi");
    arr.splice(-2, 1, "pear");
    console.log(arr.shift());
    arr.unshift("apricot", "peach");
});
