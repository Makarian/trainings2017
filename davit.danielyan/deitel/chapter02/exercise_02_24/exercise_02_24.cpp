#include <iostream>

int
main() 
{ 
    int number;
    std::cout << "Type number: ";
    std::cin >> number;
    if (number % 2 == 0) {
        std::cout << number << " is even. " << std::endl;
	return 0;    
    }
    std::cout << number << " is odd. " << std::endl;

    return 0;
}

