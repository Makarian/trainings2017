progname1=encryption
progname2=decryption

CPP_FILES=$(wildcard *.cpp)
TEST_INPUT_FILES=$(wildcard test*.in)

O_FILES=$(patsubst  %.cpp, %.o,  $(CPP_FILES))
S_FILES=$(patsubst  %.cpp, %.s,  $(CPP_FILES))
II_FILES=$(patsubst %.cpp, %.ii, $(CPP_FILES))
TEST_TARGETS=$(patsubst %.in, %, $(TEST_INPUT_FILES))

$(progname1): $(progname1).o | $(progname2) .gitignore
	@echo "Linking..."
	g++ $^ -o $@
	@echo "Building $(progname1).cpp completed."

$(progname2): $(progname2).o | .gitignore
	@echo "Linking..."
	g++ $^ -o $@
	@echo "Building $(progname2).cpp completed."

%.o: %.s
	@echo "Compiling..."
	g++ -c $^ -o $@

%.s: %.ii
	@echo "Assembling..."
	g++ -S $^ -o $@

%.ii: %.cpp
	@echo "Pre-processing..."
	g++ -E $< -o $@

.PRECIOUS: $(S_FILES) $(II_FILES)
.SECONDARY: $(S_FILES) $(II_FILES)

.gitignore:
	echo $(progname1) > $@
	echo $(progname2) >> $@

clean:
	@echo "Cleaning..."
	rm *.ii *.s *.o $(progname1) $(progname2) *.out .gitignore

test: $(TEST_TARGETS)
	echo "All tests passed successfully."

test%: $(progname1) $(progname2)
	echo "Testing $@..."
	./$(progname1) < $@.in > $@.out || echo "$(progname1) returned non-zero"
	diff $@.out $@.expected1 > /dev/null 2>&1 && echo PASSED || echo FAILED
	./$(progname2) < $@.in > $@.out || echo "$(progname2) returned non-zero"
	diff $@.out $@.expected2 > /dev/null 2>&1 && echo PASSED || echo FAILED

