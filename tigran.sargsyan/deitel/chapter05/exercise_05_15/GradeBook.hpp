#include <string>

class
GradeBook
{
public:
    GradeBook(std::string courseName);
    void setCourseName(std::string courseName);
    std::string getCourseName();
    void displayMessage();
    void inputGrades();
    void displayGradeReport();
    void displayAverageGrade();
private:
    std::string courseName_;
    int aCount_;
    int bCount_;
    int cCount_;
    int dCount_;
    int fCount_;
};

