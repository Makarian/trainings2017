a.  False. C++ applies the operators in arithmetic expressions in a specific order 
           determined by the following precedence.
  
           1. Operations within the pairs of parentheses are
              evaluated first.
           2. Multiplication, division and modules operations are applied next.
           3. Addition and subtraction operations are applied next.
           4. Equality operation is applied at the very end.

b.  True.

c.  False. We should write this kind of statement like this  a = 5;

d.  False. Multiplication, division and modules operations are applied first
           from left to right and addition, subtraction operations are applied last
           from left to right.

e.  False. h22 is a valid variable name.
