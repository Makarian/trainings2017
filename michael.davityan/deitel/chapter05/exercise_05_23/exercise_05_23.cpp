#include <iostream>
#include <cmath>

int
main()
{
    const int RHOMBUS_SIZE = 9;
    const int rowLimit = RHOMBUS_SIZE / 2;
    for (int row = -rowLimit; row <= rowLimit; ++row) {
        const int columnLimit = 4 - std::abs(row); 
        for (int column = -rowLimit; column <= columnLimit; ++column) {
            std::cout << (column < -columnLimit ? " " : "*");
        }
        std::cout << std::endl;
    }
    return 0;
}

