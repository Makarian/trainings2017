#include <iostream>
#include <iomanip>
#include <cmath>

int
main()
{
    std::cout << "=======================================================================\n";
    std::cout << "|--------------       NUMBER SYSTEMS TABLE CONTENT...     ------------|\n";
    std::cout << "=======================================================================\n";
    std::cout << "|" << std::setw(17) << "decimal|" << std::setw(22) << "binary|" 
              << std::setw(16) << "octal|" << std::setw(15) << "hexadecimal|" << std::endl;
    std::cout << "=======================================================================\n";
    const int DECIMAL_LIMIT = 256;
    for (int number = 1; number <= DECIMAL_LIMIT; ++number) {
        std::cout << "|" << std::setw(16) << number << "|";
        const int SYSTEM_LIMIT = 16;
        for (int systemType = 2; systemType <= SYSTEM_LIMIT; systemType *= 2) {
            if (4 == systemType) {
                continue;
            }
            int denominator = SYSTEM_LIMIT / systemType;
            int powerOfSystemType = std::pow(systemType, denominator + 1); 
            std::cout << std::setw(12); 
            do {
                const int variableForCalculate = number / powerOfSystemType;
                const char bitValue = variableForCalculate % systemType + '0';
                std::cout << (bitValue < ':' ? bitValue : static_cast<char>(bitValue - 57 + 96));
                powerOfSystemType /= systemType;
            } while (powerOfSystemType != 0);
            std::cout << "|";
        }
        std::cout << std::endl;
    } 
    std::cout << "=======================================================================\n";
    std::cout << "|-------------------------   end of table...   -----------------------|\n";
    std::cout << "======================================================================="
              << std::endl; 
    return 0;
}
