#include <iostream>
#include <unistd.h>
#include <iomanip>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert a whole numbers until 9999(insert 9999 if you finished): ";
    }
    int sum = 0;
    int countOfNumbers = 0;
    do {
        int eachNumber;
        std::cin >> eachNumber;
        if (9999 == eachNumber) {
            break;
        }
        if (eachNumber > 9999) {
            std::cerr << "Error 1: wrong number. Must be less than 9999.\n";
            return 1;
        }
        ++countOfNumbers;
        sum += eachNumber;
    } while (true);

    std::cout << "average is: " << static_cast<double>(sum) / countOfNumbers << std::endl;
    return 0;
}
