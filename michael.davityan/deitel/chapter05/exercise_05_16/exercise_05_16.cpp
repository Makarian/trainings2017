#include <iostream>
#include <iomanip>
#include <cmath> 

int 
main()
{
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    int amount = 100000; 
    for (int year = 1; year <= 10; ++year) {
        int rate = 5;
        amount = amount * (100 + rate) / 100;
        std::cout << std::setw(4) << year << std::setw(18) 
                  << amount / 100 << "." << amount % 100 << std::endl;
    }
    return 0; 
} 
