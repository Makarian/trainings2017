#include <iostream>
#include <unistd.h>
#include <string>

void 
stringReverse(const std::string& string, const size_t counterForReversing = 0)
{
    const int sizeOfString = string.size();
    if ('\0' != string[counterForReversing]) {
        std::cout << string[(sizeOfString - 1) - counterForReversing];
        return stringReverse(string, counterForReversing + 1);
    }
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program takes string from user\n" 
                  << "and prints it reversed.\n"
                  << "insert string: ";
    }
    std::string string;
    std::getline(std::cin, string);
    std::cout << "reversed: ";
    stringReverse(string);
    std::cout << std::endl;
    return 0;
}

