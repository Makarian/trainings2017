#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <vector>
#include <cassert>
#include <limits>

template <typename T>
T
recursiveMinimum(const std::vector<T>& vector, const int sizeOfVector,
                T minimum = std::numeric_limits<T>::max())
{
    assert(sizeOfVector >= 0);
    if (0 == sizeOfVector) {
        return minimum;
    }
    if (minimum > vector[sizeOfVector]) {
        minimum = vector[sizeOfVector];
    }
    return recursiveMinimum(vector, sizeOfVector - 1, minimum);
}

template <typename T>
void
inputVector(std::vector<T>& vector, const int sizeOfVector)
{
    assert(sizeOfVector > 0);
    for (int index = 0; index < sizeOfVector; ++index) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "element " << index + 1 << ": ";
        }
        std::cin >> vector[index];
    }
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert integers vector size and elements.\n" << "size: ";
    }
    int integersVectorSize;
    std::cin >> integersVectorSize;
    if (integersVectorSize < 0) {
        std::cerr << "Error 1: size must be more than zero.\n";
        return 1;
    }
    std::vector<int> integersVector(integersVectorSize);
    inputVector(integersVector, integersVectorSize);
    std::cout << "Minimum is: " << recursiveMinimum(integersVector, integersVectorSize - 1);
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert floating numbers vector size and elements.\n" << "size: ";
    }
    int floatingVectorSize;
    std::cin >> floatingVectorSize;
    if (floatingVectorSize < 0) {
        std::cerr << "Error 1: size must be more than zero.\n";
        return 1;
    } 
    std::vector<double> floatingVector(floatingVectorSize);
    inputVector(floatingVector, floatingVectorSize);
    std::cout << "Minimum is: " << recursiveMinimum(floatingVector, floatingVectorSize - 1); 
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert characters vector size and elements.\n" << "size: ";
    }
    int charactersVectorSize;
    std::cin >> charactersVectorSize;
    if (charactersVectorSize < 0) {
        std::cerr << "Error 1: size must be more than zero.\n";
        return 1;
    }
    std::vector<char> charactersVector(charactersVectorSize);
    inputVector(charactersVector, charactersVectorSize);
    std::cout << "Minimum is: " << recursiveMinimum(charactersVector, charactersVectorSize - 2);
    std::cout << std::endl;
    return 0;
}
