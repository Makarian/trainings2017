#include <iostream>
#include <unistd.h>
#include <string>

bool 
palindromeCheck(const std::string& word, size_t lastIdx, size_t firstIdx = 0) 
{
    if (firstIdx >= lastIdx) {
        return true;
    }
    if (' ' == word[firstIdx] || '\t' == word[firstIdx]) {
        return palindromeCheck(word, lastIdx, firstIdx + 1);
    }
    if (' ' == word[lastIdx] || '\t' == word[lastIdx]) {
        return palindromeCheck(word, lastIdx - 1, firstIdx);
    }
    if (word[firstIdx] != word[lastIdx]) {
        return false;
    }
    return palindromeCheck(word, lastIdx - 1, firstIdx + 1);
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program takes from user a word\n" 
                  << "and check if it is palindrome or not.\n"
                  << "If yes then the program returns 1 otherwise 0.\n"
                  << "Input the word: ";
    }
    std::string word;
    std::getline(std::cin, word);
    const size_t lastIdx = word.size() - 1;
    if (palindromeCheck(word, lastIdx)) {
        std::cout << "The string is palindrome\n";
    } else {
        std::cout << "The string is not palindrome" << std::endl;
    }
    return 0;
}
