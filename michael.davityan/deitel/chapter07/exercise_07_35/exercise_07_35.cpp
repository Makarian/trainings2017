#include <iostream>
#include <iomanip>
#include <unistd.h>

template <typename T>
void 
printArray(const T array[], const size_t sizeOfArray, const size_t firstIdx = 0) 
{
    if (firstIdx < sizeOfArray) {
        std::cout << array[firstIdx] << " ";
        return printArray(array, sizeOfArray, firstIdx + 1);
    }
}

template <typename T>
void 
inputNumericArray(T array[], const size_t sizeOfArray) 
{
    for (size_t index = 0; index < sizeOfArray; ++index) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "element " << index + 1 << ": ";
        }
        std::cin >> array[index];
    }
}

void 
inputCharArray(char array[], const size_t sizeOfArray) 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "word: ";
    }
    std::cin >> std::setw(sizeOfArray) >> array;
}

int
main()
{
    const int BUFFER_SIZE = 10;    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert integers array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t integersArraySize;
    std::cin >> integersArraySize;
    if (integersArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    int integersArray[BUFFER_SIZE];
    inputNumericArray(integersArray, integersArraySize);
    printArray(integersArray, integersArraySize);
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert floating numbers array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t floatingArraySize;
    std::cin >> floatingArraySize;
    if (floatingArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    double floatingArray[BUFFER_SIZE];
    inputNumericArray(floatingArray, floatingArraySize);
    printArray(floatingArray, floatingArraySize); 
    std::cout << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert characters array size and elements(size must be <= 10).\n" << "size: ";
    } 
    size_t charactersArraySize;
    std::cin >> charactersArraySize;
    if (charactersArraySize > BUFFER_SIZE) {
        std::cerr << "Error 1: size must be less or equal than 10.\n";
        return 1;
    }
    char charactersArray[BUFFER_SIZE];
    inputCharArray(charactersArray, charactersArraySize);
    printArray(charactersArray, charactersArraySize - 1);
    std::cout << std::endl;
    return 0;
}
