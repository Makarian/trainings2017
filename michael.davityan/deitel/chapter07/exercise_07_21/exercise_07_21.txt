Question!!!.
------------------------------------------------------------------------------------------------------
What does the following program do?
 #include <iostream>
 using std::cout;
 using std::endl;

 void someFunction( int [], int, int ); // function prototype

 int 
 main()
 {
    const int arraySize = 10;
    int a[ arraySize ] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    cout << "The values in the array are:" << endl;
    someFunction( a, 0, arraySize );
    cout << endl;
    return 0; // indicates successful termination
 } // end main

 void 
 someFunction( int b[], int current, int size )
 {
    if ( current < size ) {
        someFunction( b, current + 1, size );
        cout << b[ current ] << " ";
    }
 }
======================================================================================================


Answer!!!.
This program by the function someFunction prints a[] array all elements value from 10 to 1 with 
recursive method, like this.....

The values in the array are:
10 9 8 7 6 5 4 3 2 1

