Question!!!!.
Determine whether the following program segments contain errors. For each error, explain how it can be corrected. [Note: For a particular program segment, it is possible that no errors are present in the segment.]
--------------------------------------------------------
a.  template < class A >
    int sum( int num1, int num2, int num3 )
    {
        return num1 + num2 + num3;
    }
--------------------------------------------------------
b.  void printResults( int x, int y )
    {
        cout << "The sum is " << x + y << '\n';
        return x + y;
    }
--------------------------------------------------------
c.  template < A >
    A product( A num1, A num2, A num3 )
    {
        return num1 * num2 * num3;
    }
--------------------------------------------------------
d.  double cube( int );
    int cube( int );
--------------------------------------------------------

Answer!!!!.
--------------------------------------------------------
a.  template < class A >
    A sum(A num1,A num2,A num3 ). ///function template must have a returning type A. Arguments also the same!!!!.
    {
        return num1 + num2 + num3;
    }
--------------------------------------------------------
b.  void printResults( int x, int y )
    {
        cout << "The sum is " << x + y << '\n';
        return x + y; /// wrong. function with return type void can't return integer value. 
        I think this line must be deleted.
    }
--------------------------------------------------------
c.  template <class A > /// class mising.....
    A product( A num1, A num2, A num3 )
    {
        return num1 * num2 * num3;
    }
--------------------------------------------------------
d.  double cube( int ); ///I think this is logical error. Much better if function have the same returning type 
as argument type....
    int cube( int );
--------------------------------------------------------
