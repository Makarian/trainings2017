#include <iostream>
#include <unistd.h>
#include <cassert>

int qualityPoints(const int averageGrade);

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input student average: ";
    }
    int averageGrade;
    std::cin >> averageGrade;
    if ((averageGrade < 0) || (averageGrade > 100)) {
        std::cerr << "Error 1: must be more than 0 or less than 100.\n";
        return 1;
    }
    std::cout << qualityPoints(averageGrade) << std::endl;

    return 0;
}

int 
qualityPoints(const int averageGrade)
{
    assert((averageGrade >= 0) && (averageGrade <= 100));
    if (100 == averageGrade) {
        return 4;
    }
    int result = averageGrade / 10 - 5;
    return (result > 0 ? result : 0);
}
