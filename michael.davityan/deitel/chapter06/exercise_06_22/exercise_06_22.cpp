#include <iostream>
#include <cassert>

void square(const int side);

int
main()
{
    int side;
    std::cin >> side;
    if (side < 0) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    }
    square(side);

    return 0;
}

void
square(const int side) 
{
    assert(side >= 0);
    for (int row = 1; row <= side; ++row) {
        for (int column = 1; column <= side; ++column) {
            std::cout << "* ";
        }
        std::cout << std::endl;
    }
}
