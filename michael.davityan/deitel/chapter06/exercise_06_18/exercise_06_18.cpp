#include <iostream>
#include <cstdlib>

double integerPower(const int base, const int exponent);

int 
main()
{
    int base;
    std::cin >> base;
    int exponent;
    std::cin >> exponent;
    std::cout << integerPower(base, exponent) << std::endl;

    return 0;
}

double
integerPower(const int base, const int exponent) 
{
    double totalValue = 1.0;
    int powerLimit = std::abs(exponent);
    for (int power = 1; power <= powerLimit; ++power) {
        totalValue *= base;
    }
    if (exponent < 0) {
        return 1.0 / totalValue;
    }
    return totalValue;
}

