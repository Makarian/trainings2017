#include <iostream>
#include <cmath>
#include <cassert>

double hypotenuse(const double triangleEdge1, const double triangleEdge2);

int
main() 
{
    std::cout << "Triangle\t" << "Edge1\t" << "Edge2\t" << "Hypotenuse\n";

    for (int triangle = 1; triangle <= 3; ++triangle) {
        double triangleEdge1;
        std::cin >> triangleEdge1;
        double triangleEdge2;
        std::cin >> triangleEdge2;
        if ((triangleEdge1 <= 0) || (triangleEdge2 <= 0)) {
            std::cout << "Error 1: wrong edge." << std::endl;
            return 1;
        } 

        std::cout << "   " << triangle << "\t\t  " << triangleEdge1 << "\t  " << triangleEdge2 << "\t    "
                  << hypotenuse(triangleEdge1, triangleEdge2) << std::endl;
    }
    
    return 0;
}

double 
hypotenuse(const double triangleEdge1, const double triangleEdge2)
{       
    assert((triangleEdge1 > 0) && (triangleEdge2 > 0));
    return std::sqrt(triangleEdge1 * triangleEdge1 + triangleEdge2 * triangleEdge2);
}

