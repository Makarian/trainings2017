#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

void printMessage(int answer, int number);
void printLowMessage();
void printHighMessage();
void printGuessMessage();
void printNewGameMessage();

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    } 
    if (::isatty(STDIN_FILENO)) {
        std::cout << "I have a number between 1 and 1000.\n" 
                  << "Can you guess my number?\n"
                  << "Please type your first guess. ";
    }
    int number = 0;
    bool answerStatus = true;
    while (true) {
        if (answerStatus) {
            number = 1 + std::rand() % 1000;
            std::cout << number;
        }
        int answer;
        std::cin >> answer;
        answerStatus = (number == answer);
        printMessage(answer, number);
        if (answerStatus) {
            char answerForContinuation;
            std::cin >> answerForContinuation;
            if ((answerForContinuation != 'n') && (answerForContinuation != 'N') &&
                (answerForContinuation != 'y') && (answerForContinuation != 'Y')) {
                std::cerr << "Error 1: wrong answer. Must be y or n(lowercase or uppercase).\n";
                return 1; 
            }
            if (('n' == answerForContinuation) || ('N' == answerForContinuation)) {
                break;
            }
            if (('y' == answerForContinuation) || ('Y' == answerForContinuation)) {
                if(::isatty(STDIN_FILENO)) {
                    printNewGameMessage();
                }
            }
        }        
    }
    return 0;
}

void 
printMessage(int answer, int number)
{
    if (answer < number) {
        printLowMessage();
    } 
    if (answer > number) {
        printHighMessage();
    }
    if (answer == number) {
        printGuessMessage();
    }
}

void 
printLowMessage()
{
    std::cout << "Too low. Try again: ";
}

void 
printHighMessage()
{
    std::cout << "Too high. Try again: ";
}

void 
printGuessMessage()
{
    std::cout << "Excellent! You guessed the number!\n"
              << "Would you like to play again (y or n)?: ";
}

void 
printNewGameMessage()
{
    std::cout << "Can you guess my new number? ";
}
