#include <iostream>
#include <cmath>
#include <unistd.h>

double distanceBetweenTwoPoints(const double x1, const double y1, const double x2, const double y2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert first  point x cordinat. ";
    }
    double x1;
    std::cin >> x1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert first  point y cordinat. ";
    }
    double y1;
    std::cin >> y1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second point x cordinat. ";
    }
    double x2;
    std::cin >> x2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second point y cordinat. ";
    }
    double y2;
    std::cin >> y2;
    std::cout << "The distance between two points is: " << distanceBetweenTwoPoints(x1, y1, x2, y2) 
              << std::endl;
    return 0;
}

double 
distanceBetweenTwoPoints(const double x1, const double y1, const double x2, const double y2)
{
    double xCordinatsDifference = x2 - x1;
    double yCordinatsDifference = y2 - y1;
    double distance = std::sqrt(xCordinatsDifference * xCordinatsDifference + yCordinatsDifference * yCordinatsDifference);
    return distance;
}
