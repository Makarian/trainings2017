#include "Gradebook.hpp"
#include <iostream>
#include <string>

GradeBook::GradeBook(std::string courseName, std::string teacherName)
{
    setCourseName(courseName);
    setTeacherName(teacherName); 
} 

void 
GradeBook::setCourseName(std::string courseName)
{
    courseName_ = courseName; 
}

void
GradeBook::setTeacherName(std::string teacherName)
{
    teacherName_ = teacherName; 
}

std::string 
GradeBook::getCourseName()
{
    return courseName_; 
}

std::string
GradeBook::getTeacherName()
{
    return teacherName_;
}

void 
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!" << "\n"
              << "This course is presented by: " << getTeacherName() << std::endl;
}
