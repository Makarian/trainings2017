#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert ten different integers.\n";
    }

    int counter = 1;
    int largest = INT_MIN;
    while (counter <= 10) {
        int number;
        std::cin >> number;
        if (largest < number) {
            largest = number;
        }
        ++counter;    
    }
    std::cout << "Largest is " << largest << std::endl;

    return 0;
}
