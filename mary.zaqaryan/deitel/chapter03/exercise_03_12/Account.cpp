#include "Account.hpp"
#include <iostream>

Account::Account(int initialBalance)
{
    setAccountBalance(initialBalance);
}

void
Account::setAccountBalance(int accountBalance)
{
    if (accountBalance > 0) {
        accountBalance_ = accountBalance;
        return;
    }
    accountBalance_ = 0;
    std::cout << "Warning 1: Invalid initial balance\n";
}

int
Account::credit(int addingAmount)
{
    accountBalance_ = accountBalance_ + addingAmount;
    return accountBalance_;
}

int 
Account::debit(int reducingAmount)
{
    if (reducingAmount > accountBalance_) {
        std::cout << "Warning 2: Debit ammount exceedes account balance.\n";
        return accountBalance_;
    }
    accountBalance_ = accountBalance_ - reducingAmount;
    return accountBalance_;
}

int
Account::getAccountBalance()
{
    return accountBalance_;
}

