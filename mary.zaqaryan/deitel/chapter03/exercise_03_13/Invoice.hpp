#include <string>

class Invoice
{
public:
    Invoice(std::string number, std::string description, int quantity, int price);
    void setNumber(std::string number);
    void setDescription(std::string description);
    void setQuantity(int quantity);
    void setPrice(int price);
    std::string getNumber();
    std::string getDescription();
    int getQuantity();
    int getPrice();
    int getInvoiceAmount();

private:
    std::string number_;
    std::string description_;
    int quantity_;
    int price_;
};

