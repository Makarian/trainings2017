#include "Invoice.hpp"
#include <iostream>
#include <string>

int
main()
{
    Invoice item("N1", "Ice Cream", 500, 150);///number, description, quantity, price
    std::cout << "Description of Item: " << item.getDescription() << std::endl;
    std::cout << "Item's number is: " << item.getNumber() << std::endl;
    std::cout << "Item's price is: " << item.getPrice() << std::endl;
    std::cout << "Item's amount is: " << item.getInvoiceAmount() << std::endl;

    int newQuantity = -20;
    item.setQuantity(newQuantity);
    std::cout << "Changed quantity is: " << item.getQuantity() << std::endl;
    
    return 0;
}
