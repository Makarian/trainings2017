#include "Employee.hpp"
#include <iostream>
#include <string>

int
main()
{
    Employee employee1("Sedrak", "Baxdasaryan", -150000);
    Employee employee2("Arpine", "Aloyan" , 290000);
    
    std::cout << "Employee1's yearly salary is " << employee1.getEmployeeSalary() << ".\n";
    std::cout << "Employee2's yearly salary is " << employee2.getEmployeeSalary() << ".\n";

    int employee1Salary = employee1.getEmployeeSalary();
    int employee1IncreasedSalary = employee1Salary + (employee1Salary * 10 / 100);
    employee1.setEmployeeSalary(employee1IncreasedSalary);
    std::cout << "After increasing by 10 percent employee1's salary is " << employee1.getEmployeeSalary() << ".\n";
    
    int employee2Salary = employee2.getEmployeeSalary();
    int employee2IncreasedSalary = employee2Salary + (employee2Salary * 10 / 100);
    employee2.setEmployeeSalary(employee2IncreasedSalary);
    std::cout << "After increasing by 10 percent employee2's salary is " << employee2.getEmployeeSalary() << ".\n";
    
    return 0;
}
