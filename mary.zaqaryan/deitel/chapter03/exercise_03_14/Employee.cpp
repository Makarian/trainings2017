#include "Employee.hpp"
#include <iostream>
#include <string>

Employee::Employee(std::string employeeName, std::string employeeSurname, int employeeSalary)
{ 
    setEmployeeName(employeeName);
    setEmployeeSurname(employeeSurname);
    setEmployeeSalary(employeeSalary);
}

void 
Employee::setEmployeeName(std::string employeeName)
{
    employeeName_ = employeeName;
}

std::string 
Employee::getEmployeeName()
{
    return employeeName_;
}

void 
Employee::setEmployeeSurname(std::string employeeSurname)
{
    employeeSurname_ = employeeSurname;
}

std::string
Employee::getEmployeeSurname()
{
    return employeeSurname_;
}

void 
Employee::setEmployeeSalary(int employeeSalary)
{
    if (employeeSalary < 0) {
        employeeSalary_ = 0;
        return;
    }
    employeeSalary_ = employeeSalary;
}

int 
Employee::getEmployeeSalary()
{
    return employeeSalary_;
}

