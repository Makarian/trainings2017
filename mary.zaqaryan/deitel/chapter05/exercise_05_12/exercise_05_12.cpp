#include <iostream>

int
main()
{
    for (int i = 1; i <= 10; i++) {
        for (int j = 1; j <= i; j++) {
            std::cout << "*";
        }
        std::cout << std::endl;
    };

    std::cout << std::endl;

    for (int i = 1; i <= 10; i++) {
        for (int j = 10; j >= i; j--) {
            std::cout << "*";
        }
        std::cout << std::endl;
    };

    std::cout << std::endl;

    for (int i = 1; i <= 10; i++) { 
        for (int j = 1; j < i; j++) {
            std::cout << " ";    
        }
        for (int k = 10; k >= i; k--) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int i = 1; i <= 10; i++) {
        for (int j = 10; j > i; j--) {
            std::cout << " ";
        }
        for (int k = 1; k <= i; k++) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    
    std::cout << std::endl;
    
    int n = 10;
    for (int i = 1; i <= 10; i++) {
        
        for (int j = 1; j <= i; j++) {
            std::cout << "*";
        }

        for (int k = n; k >= 1; k--) {
            std::cout << " ";
        }

        for (int j = n; j >= 1; j--) {
            std::cout << "*";
        }

        for (int k = 1; k <= i; k++) {
            std::cout << " ";
        }

        for (int j = 1; j <= i; j++) {
            std::cout << " ";
        }

        for (int k = n; k >= 1; k--) {
            std::cout << "*";
        }

        for (int j = n; j >= 1; j--) {
            std::cout << " ";
        }

        for (int k = 1; k <= i; k++) {
            std::cout << "*";
        }
        std::cout << std::endl;
        --n;
    }
}
