We declared number variable as integer. 
The largest number we can put in integer is -2147483648 (2^31 - 1).
And when integer's size (4 byte) is full, code prints endless 0s.
