#include <iostream>

int 
main()
{   
    int number;

    std::cout << "Enter integer number: ";
    std::cin >> number;

    int reverseNumber = 0;
    int usedNumber = number;
    while (usedNumber != 0) {
        int digit;
        digit = usedNumber % 10;
        reverseNumber = reverseNumber * 10 + digit;
        usedNumber /= 10;
    }
    
    if (reverseNumber == number) {
        std::cout << "Your number is polindrome.";
    } else {
        std::cout << "Your number is not polindrome.";
    }
    return 0; 
}
