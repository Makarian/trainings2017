#include <iostream>
#include <iomanip>

int
main()
{
    int accountNumber = 0;
    while (true) {

        std::cout << "Enter account number (-1 to end): ";
        std::cin >> accountNumber;
        if (-1 == accountNumber){
            return 0;
        }
        if (accountNumber < -1) {
            std::cerr << "Error 1: the account number cannot have negative value." << std::endl;
            return 1;
        }

        double beginBalance = 0;
        std::cout << "Enter begining balance: ";
        std::cin >> beginBalance >> std::setprecision(2) >> std::fixed;
        if (beginBalance < 0) {
            std::cerr << "Error 2; the begining balance cannot be a negative number" << std::endl;
            return 2;
        }	

        double totalCharges = 0;
        std::cout << "Enter total charges: ";
        std::cin >> totalCharges >> std::setprecision(2) >> std::fixed;
        if (totalCharges < 0) {
            std::cerr << "Error 3; the total balance cannot be a negative number" << std::endl;
            return 3;
        }	

        double totalCredits = 0;
        std::cout << "Enter total credits: ";
        std::cin >> totalCredits >> std::setprecision(2) >> std::fixed;
        if (totalCredits < 0) {
            std::cerr << "Error 4; the total credit cannot be a negative number" << std::endl;
            return 4;
        }

        double creditLimit = 0;
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit >> std::setprecision(2) >> std::fixed;
        if (creditLimit < 0) {
            std::cerr << "Error 5; the credit limit cannot be a negative number" << std::endl;
            return 5;
        }

        double newBalance = beginBalance + totalCharges - totalCredits;
        std::cout << "New balance is " << newBalance << std::setprecision(2) << std::fixed << std::endl;
        if (newBalance < creditLimit) {
            std::cout << "Account: " << accountNumber << "\n";
            std::cout << "Credit limit: " << creditLimit << "\n" << "Balance: " << newBalance << std::setprecision(2) << std::fixed << std::endl;
            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    } 
    return 0;
}
		  
