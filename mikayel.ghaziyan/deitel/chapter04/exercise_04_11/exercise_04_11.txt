///Mikayel Ghaziyan
/// 2/26/2018
/// Exercise 4.11

a. if (age >= 65); ///	No ";"
		cout << "Age is greater than or equal to 65" << endl;
	else
		cout << "Age is less than 65 << endl"; /// (") shoudl be after ...less than 65.

b. if (age >= 65)
		cout << "Age is greater than or equal to 65" << endl;
	else  /// No  ";" after 65
		cout << "Age is less than 65" << endl; ///Again (") is supposed to be after ...less than 65.

c. int x = 1, total; /// total must be assigned to a value
	while (x <= 10)
	{
	total += x;
	x++;
	}

d. while (x <= 100)
	{
	total += x;
	x++; /// If x and total were not declared then they must be declared 
	}

e. while (y > 0)
	{
	cout << y << endl;
	y++; /// The variable y should be assigned to a value, otherwise the loop will remain infinite.
   /// y must be a decrementing value (y--), otherwise it`ll be either a infinite loop (if the initial value is a positive number) or the loop will not start at all (if the value of y is a negative number).	
	}

/// Regarding styling, all statements do not have ({}) and (std::)`s.
