#include "Analysis.hpp"
#include <iostream>
#include <string>

void 
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCount = 0;

    while(studentCount < 10) {
        int result;
        std::cout << "Enter result (1 = pass, 2 = fail): " << std::endl;
        std::cin >> result;

        if (1 == result) {
            ++passes;
            ++studentCount;
            
        } else if (2 == result) {
            ++failures;
            ++studentCount;
        } else {
            std::cout << "Warning 1. Enter 1 for the pass or 2 for the failure" << std::endl;
        }

    }

    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;

    if (8 < passes) {
        std::cout << "Raise tuition " << std::endl;
    }
}

