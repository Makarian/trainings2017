#include <iostream>
#include <iomanip>

int
main()
{	
    int x;
    int y;
	
    /// a
    std::cout << "a" << std::endl;
    std::cin >> y;
    std::cin >> x;

    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
        }
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
    }

    /// b
    std::cout << "b" << std::endl;
    std::cin >> y;
    std::cin >> x;
	
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

    /// c
    std::cout << "c" << std::endl;
    std::cin >> y;
    std::cin >> x;

    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
        }
        std::cout << "&&&&&" << std::endl;
    }

    /// d
    std::cout << "d" << std::endl;
    std::cin >> y;
    std::cin >> x;

    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

    return 0;
}
		  
