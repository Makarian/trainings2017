#include "Date.hpp"
#include <iostream>
#include <string>

int 
main()
{    
    int month = 2;
    int day = 12;
    int year = 1982;

    Date time(month, day, year);

    time.displayDate();

    return 0;
}

