#include <iostream>

int
main()
{
    int counter;

    for (counter = 1; counter < 5; ++counter) {
        std::cout << counter << " ";
    }

    std::cout << std::endl 
        << "Broke out of loop at count = " 
        << counter
        << std::endl;

    return 0;
}

