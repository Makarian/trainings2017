i =1; j = 2; k = 3; m = 2;

a)std::cout << (i == 1) << std::endl; ///1 brackets needed

b)std::cout << (j == 3) << std::endl; ///0 brackets needed

c)std::cout << (i >=1 && j < 4) << std::endl; ///1 brackets needed

d)std::cout << (m <= 99 && k < m) << std::endl; ///0 brackets needed

e)std::cout << (j >= i || k == m) << std::endl; ///1 brackets needed

f)std::cout << (k + m < j || 3 - j >= k) << std::endl; ///0 brackets needed

g)std::cout << (!m) << std::endl; ///0  no brackets needed

h)std::cout << (!(j - m)) << std::endl; ///1 inner brackets needed

i)std::cout << (!(k > m)) << std::endl; ///0 inner brackets needed

