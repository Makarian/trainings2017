#include <string>

class Employee
{
public:
    Employee(std::string firstName, std::string lastName, int monthSalary);
    void setFirstName(std::string firstName);
    std::string getFirstName();
    void setLastName(std::string lastName);
    std::string getLastName();
    void setMonthSalary(int salary);
    int getMonthSalary();

private:
    std::string firstName_;
    std::string lastName_;
    int monthSalary_;
};
