Determine whether each of the following is true or false. If false, explain why.
a. To refer to a particular location or element within an array, we specify the name of the array and the value of the particular element.
    false - not value but index
b. An array declaration reserves space for the array. - true
c. To indicate that 100 locations should be reserved for integer array p , the programmer writes the declaration p[ 100 ] 
    false - element value is missing, must be int p[100]
d. A for statement must be used to initialize the elements of a 15-element array to zero.
    false - must be written e.i. int d[15] = {0};
e. Nested for statements must be used to total the elements of a two-dimensional array. - true
