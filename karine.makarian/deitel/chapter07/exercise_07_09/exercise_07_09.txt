Consider a 2-by-3 integer array t .
a. Write a declaration for t . - int t[2][3];
b. How many rows does t have? - 2
c. How many columns does t have? - 3
d. How many elements does t have? - 2 * 3 = 6
e. Write the names of all the elements in row 1 of t . - t[1][0], t[1][1], t[1][2]
f. Write the names of all the elements in column 2 of t . - t[0][2], t[1][2]
g. Write a single statement that sets the element of t in row 1 and column 2 to zero. - t[1][2] = 0
h. Write a series of statements that initialize each element of t to zero. Do not use a loop.
    t[0][0] = 0;
    t[0][1] = 0;
    t[0][2] = 0;
    t[1][0] = 0;
    t[1][1] = 0;
    t[1][2] = 0;
i. Write a nested for statement that initializes each element of t to zero. 
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            t[i][j] = 0;
        }
    }
j. Write a statement that inputs the values for the elements of t from the terminal. 
    int input_value; //input value must have the same  type as array t e. i. int 
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cin >> input_value;
            t[i][j] = input_value;
        }
    }

k. Write a series of statements that determine and print the smallest value in array t .
    int smallest = 0; // e. i. array value type is int 
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (t[i][j] < smallest) {
                smallest = t[i][j];
            }
        }
    }
    std::cout << "Smallest is " << smallest;

l. Write a statement that displays the elements in row 0 of t .
    for (int j = 0; j < 3; ++j) {
        std::cout << t[0][j] << " ";
    }


m. Write a statement that totals the elements in column 3 of t .
    (same type as t) total = 0;
    for (int j = 0; j < 2; ++j) {
        total += t[j][3] << " ";
    }

n. Write a series of statements that prints the array t in neat, tabular format. List the column subscripts as headings across the top and
   list the row subscripts at the left of each row. 
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cout << "\t" << t[i][j];
        }
        std::cout << endl;
    }
    
