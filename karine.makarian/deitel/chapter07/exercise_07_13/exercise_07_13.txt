Write single statements that perform the following one-dimensional array operations:

a. Initialize the 10 elements of integer array counts to zero.
    int counts[10] = {0};
b. Add 1 to each of the 15 elements of integer array bonus .
    int bonus[15];
    for (int i = 0; i < 15; ++i) {
        bonus[i] =+ 1;
    }
c. Read 12 values for double array monthly Temperatures from the keyboard.
    double monthlytemperatures[15];
    for (int i = 0; i < 12; ++i) {
        std::cin >> monthlyTemperatures[i];
    }

d. Print the 5 values of integer array bestScores in column format.
    for (int i = 0; i < 5; ++i) {
        std::cout << bestScores[i] << '\t';
    }


