#include <iostream>
#include <cmath>

double
calculateCharges(const double hours) 
{
    double amount = 2;

    if (hours > 3) {
        amount = 2 + std::ceil(hours - 3) * 0.5;
    }
  
    if (amount > 10) {
        amount = 10;
    }

    return amount;
}
    
int
main()
{
    double totalHours = 0.0;
    double totalAmount = 0.0;

    for (int i = 1; i <= 3; ++i) {
        double carHours;
        std::cout << "Enter the parking hours of a car: ";
        std::cin >> carHours;
        std::cout << std::endl;

        if (carHours < 0 || carHours > 24) {
            std::cerr << "Error 1: Invalid hour." << std::endl;
            return 1;
        }  
   
        const double amount = calculateCharges(carHours);
        totalHours += carHours;
        totalAmount += amount;

        std::cout << "Car:" << i << "\t" 
                  << "Hours:" << carHours << "\t" 
                  << "Charge:" << amount << std::endl
                  << std::endl; 
    }

    std::cout << "Total hours:" << totalHours << "\t" << "Total amount:" << totalAmount << std::endl;
    
    return 0;
}

