#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <ctime>

std::string
printCorrectAnswer()
{
    const int answerType = 1 + std::rand() % 4;
    std::string response;
    switch(answerType) {
    case 1: response = "Very good!"; break;
    case 2: response = "Excellent!"; break;
    case 3: response = "Nice work!"; break;
    case 4: response = "Keep up the good work!"; break;        
    default: response = "Good job"; break;
    }

    return response;

}

std::string
printIncorrectAnswer() 
{
    const int answerType = 1 + std::rand() % 4;
    std::string response;
    switch(answerType) {
    case 1: response = "Please try again."; break;
    case 2: response = "Wrong.Try once more."; break;
    case 3: response = "Don't give up."; break;
    case 4: response = "No.Keep trying."; break;
    default: response = "Nooooo"; break;
    }

    return response;

}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    } 
    int first = 0;
    int second = 0;
    bool answerStatus = true;

    while (true) {
        if (answerStatus) {
            first = 1 + std::rand() % 10;
            second = 1 + std::rand() % 10;
        }
    
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much " << first << " times " << second << "? "; 
        }

        int answer;
        std::cin >> answer;

        if (-1 == answer) {
            break;
        }

        answerStatus = (first * second == answer);

        if (answerStatus) {
            std::cout << printCorrectAnswer() << std::endl; 
        } else {
            std::cout << printIncorrectAnswer() << std::endl;
        }
    }

    return 0;
}

