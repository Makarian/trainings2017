#include <iostream>
#include <cassert>
void
squareOfAsterisks(int side)
{
    assert(side > 0);
    for (int i = 1; i <= side; ++i) {
        for (int j = 1; j <= side; ++j) {
            std::cout << '*';
        }

        std::cout << std::endl;
    }
}

int
main()
{
    int size;
    std::cout << "Enter the size of a square: ";
    std::cin >> size;
    std::cout << std::endl;

    if (size <= 0) {
        std::cerr << "Error 1: Non positive size." << std::endl;
        return 1;
    }
    squareOfAsterisks(size);
    return 0;
}
