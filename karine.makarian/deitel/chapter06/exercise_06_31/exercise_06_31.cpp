#include <iostream>

int 
reverse(const int number)
{
    int numberCopy = number;
    int reversed = 0;
    while (numberCopy > 0) {
        int reminder = numberCopy % 10;
        reversed = reversed * 10 + reminder;
        numberCopy /= 10;
    }

    return reversed;
}

int
main()
{
    int number;
    std::cout << "Enter a number ";
    std::cin >> number;

    std::cout << "Reversed number is " << reverse(number) << std::endl;

    return 0;
}
