#include <iostream>

int mystery( int, int ); 

int main()
{
    int x, y;
    std::cout << "Enter two integers: ";
    std::cin >> x >> y;
    std::cout << "The result is " << mystery( x, у ) << std::endl;

    return 0; 
} 

int 
mystery( int a, int b )
{
    if ( b == 1 ) 
        return a;
    else 
        return a + mystery( a, b - 1 );
}

The program with a recursive method multiplies x to y.
For example if x = 2 and y = 4, the mystery function will return 8.
if x = 7 and y = 3, the mustery function will return 21.
This function b argument must be more than zero.

