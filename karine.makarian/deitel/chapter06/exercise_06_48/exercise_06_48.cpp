#include <iostream>
#include <cmath>

double
distance(const double x1, const double y1, const double x2, const double y2);

int
main()
{
    double x1;
    std::cout << "Insert first  point of x coordinate. ";
    std::cin >> x1;

    double y1;
    std::cout << "Insert first point of y coordinate. ";
    std::cin >> y1;

    double x2;
    std::cout << "Insert second point of x coordinate. ";
    std::cin >> x2;

    double y2; 
    std::cout << "Insert second point of y coordinate. ";
    std::cin >> y2;
    
    std::cout << "The distance between to points is " 
              << distance(x1, y1, x2, y2) << std::endl;

    return 0;
}

double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double xCoordinatesSub = x2 - x1;
    const double yCoordinatesSub = y2 - y1; 
    const double distance = std::sqrt((xCoordinatesSub * xCoordinatesSub) + (yCoordinatesSub * yCoordinatesSub));
    
    return distance;
}

