#include <iostream>

bool 
perfect(const int number)
{
    int sum = 1;
    for (int counter = 2; counter < number; ++counter) {
        if (0 == number % counter) {
            sum += counter;
        }
    }
    return number == sum;
}

int
main()
{
    int count;
    std::cout << "Enter the count ";
    std::cin >> count;

    if (count < 2) {
        std::cerr << "Error 1 : Invalid count." << std::endl;
        return 1;
    }

    for (int counter = 2; counter <= count; ++counter) {
        if (perfect(counter)) {
            std::cout << counter << "'s divisors are ";
            for (int divisor = 1; divisor < counter; ++divisor) {
                if (0 == counter % divisor){
                    std::cout << divisor << "  ";
                }
            }
            std::cout << std::endl;
        }
    }

    return 0;
}

