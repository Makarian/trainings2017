#include <iostream>

int
gcd(int number1, int number2)
{
    while(0 != number2) {
        const int residual = number1 % number2;
        number1 = number2;
        number2 = residual;
    }
    
    return number1;
}    

int
main()
{
    int number1;
    int number2;
    std::cout << "Enter 2 numbers ";
    std::cin >> number1 >> number2;

    std::cout << "The greatest common divisor is " << gcd(number1, number2) << std::endl;

    return 0;
}
