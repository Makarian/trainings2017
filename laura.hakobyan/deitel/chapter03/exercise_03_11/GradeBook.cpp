#include "GradeBook.hpp" 
#include <iostream>
#include <string>

GradeBook::GradeBook(std::string course, std::string instructor)
{
    setCourseName(course);
    setInstructorName(instructor);
}

void
GradeBook::setCourseName(std::string course)
{
    courseName_ = course;
}

void
GradeBook::setInstructorName(std::string instructor)
{
    instructorName_ = instructor;
}

std::string 
GradeBook::getCourseName()
{
    return courseName_;
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for " << getCourseName() << "!" << std::endl;
    std::cout << "This course is presented by: " << getInstructorName() << "!" << std::endl;
}

