#include <iostream>

int
main()
{
    /// a.
    /// int x,y;
    /// std::cout << "Enter x, y: ";
    /// std::cin >> x >> y;
    /// bool originalExpression = !(x < 5) && !(y >= 7);
    /// bool newExpression = !((x < 5) || (y >= 7));

    /// b.
    /// int a, b, g;
    /// std::cout << "Enter integers: ";
    /// std::cin >> a >> b >> g;
    /// bool originalExpression = !(a == b) || !(g != 5);
    /// bool newExpression = !((a == b) && (g != 5));

    /// c.
    /// int x, y;
    /// std::cout << "Enter integers: ";
    /// std::cin >> x >> y;
    /// bool originalExpression = !((x <= 8) && (y > 4));
    /// bool newExpression = !(x <= 8) || !(y > 4);

    /// d.
    int i, j;
    std::cout << "Enter integers: ";
    std::cin >> i >> j;
    bool originalExpression = !((i > 4) || (j <= 6));
    bool newExpression = !(i > 4) && !(j <= 6);

    std::cout << "Value of original Expression is " << originalExpression << std::endl;
    std::cout << "Value of new Expression is " << newExpression << std::endl;

    if (originalExpression == newExpression) {
        std::cout << "They are equivalent." << std::endl;
    } else {
        std::cout << "They aren't equivalent." << std::endl;
    }
    return 0;
}

