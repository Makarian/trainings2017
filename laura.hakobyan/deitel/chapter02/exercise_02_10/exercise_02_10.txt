a. false - All operators (in this chapter) associate from left to right except the assignment operator = (right to left)
b. True - but we should avoid variable names like _under_bar_, m928134, t5, j7 and so on.
c. False - typical example of an assignment statement is a = 5.
d. false - C ++ applies the operators in arithmetic expressions in a precise sequence determined by the rules of operator precedence.
e. True - Though h22 is started from letter, but we should avoid variable names like this.
