#include <iostream>

int 
main()
{
    double firstSide;
    std::cout << "Enter first side: ";
    std::cin  >> firstSide;
    if (firstSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1; 
    }
    double secondSide;
    std::cout << "Enter first side: ";
    std::cin  >> secondSide;
    if (secondSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1; 
    }

    double thirdSide;
    std::cout << "Enter first side: ";
    std::cin  >> thirdSide;
    if (thirdSide <= 0) {
        std::cerr << "Error1: Number can not be negative." << std::endl;
        return 1; 
    }

    if ((thirdSide * thirdSide) == ((firstSide * firstSide) + (secondSide * secondSide))) {
        std::cout << "This is a right triangle." << std::endl;
        return 0;
    }
    if ((firstSide * firstSide) == ((thirdSide * thirdSide) + (secondSide * secondSide))) {
        std::cout << "This is a right triangle." << std::endl;
        return 0;
    }
    if ((secondSide * secondSide) == ((firstSide * firstSide) + (thirdSide * thirdSide))) {
        std::cout << "This is a right triangle." << std::endl;
        return 0;
    }
    std::cout << "This is a false triangle." << std::endl;
    return 0;
}

