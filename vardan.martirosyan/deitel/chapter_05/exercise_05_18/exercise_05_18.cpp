#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "Decimal\t\tBinary\t\tOctal\t\tHexal" << std::endl;

    for (int decimal = 1; decimal <= 256; ++decimal) {
        std::cout << std::dec << decimal << "\t\t";

        int number = decimal;
        for (int  index = 256; index > 0; index /= 2) {
            if (index <= number) {
                std::cout << "1";
                number -= index;
            } else {
                std::cout << "0";
            }
        }
        std::cout << "\t\t"; 

        std::cout << std::oct << decimal << "\t\t";
        std::cout << std::hex << decimal << std::endl;
    }
    return 0;
}

