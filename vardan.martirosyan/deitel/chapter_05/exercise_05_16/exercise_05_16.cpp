#include <iostream> 
#include <iomanip> 
 
int main()
{
    int principal = 100000;

    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);
 
    for (int year = 1; year <= 10; ++year) {
        principal = (principal  * 105) / 100;
        
        int dollar = principal / 100;
        int cent = principal % 100; 
       
        std::cout << std::setw(4) << year << std::setw(18) << dollar << "." << cent << std::endl;
    }
    return 0;
} 

