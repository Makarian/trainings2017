a. Meaningful comments are used to document a program and improve it's readability.
b. The object used to print information on the screen is “std::cout”.
c. A C++ statement that makes a decision is “if statement”.
d. Most calculations are normally preformed by arithmetic statements (addition, subtraction,    multiplication, division, modulus).
e. The “std::cin” object inputs values from the keyboard.

