#include <iostream>
#include <cassert>
#include <cmath>

int userClockTimeInSeconds(const int hours, const int minutes, const int seconds);

int 
main()
{
    int firstTime;
    std::cout << "Enter first time: ";
    std::cin  >> firstTime;
    if (firstTime < 0 || firstTime > 23) {
        std::cout << "Error 1: Invalid Time. " << std::endl;
        return 1;
    }
    int firstMinute;
    std::cout << "Enter first minute: ";
    std::cin >> firstMinute;
    if (firstMinute < 0 || firstMinute > 59) {
        std::cout << "Error 1: Invalid Minute. " << std::endl;
        return 2;
    }
    int firstSecond;
    std::cout << "Enter first second: ";
    std::cin >> firstSecond;
    if (firstSecond < 0 || firstSecond > 59) {
        std::cout << "Error 1: Invalid second. " << std::endl;
        return 1;
    }
    std::cout << firstTime << ":" << firstMinute << ":" << firstSecond << std::endl;

    int secondTime;
    std::cout << "Enter second time: ";
    std::cin  >> secondTime;
    if (secondTime < 0 || secondTime > 23) {
        std::cout << "Error 1: Invalid Time. " << std::endl;
        return 1;
    }
    int secondMinute;
    std::cout << "Enter second minute: ";
    std::cin >> secondMinute;
    if (secondMinute < 0 || secondMinute > 59) {
        std::cout << "Error 1: Invalid Minute. " << std::endl;
        return 2;
    }
    int secondSecond;
    std::cout << "Enter second second: ";
    std::cin >> secondSecond;
    if (secondSecond < 0 || secondSecond > 59) {
        std::cout << "Error 1: Invalid second. " << std::endl;
        return 1;
    }
    std::cout << secondTime << ":" << secondMinute << ":" << secondSecond << std::endl;

    const int firstResult  = userClockTimeInSeconds(firstTime, firstMinute, firstSecond);
    const int secondResult = userClockTimeInSeconds(secondTime, secondMinute, secondSecond);
    std::cout << "First result is: " << firstResult << "\n"
              << "Second result is: " << secondResult << "\n"
              << "Difference result is: " << std::abs(secondResult - firstResult) << std::endl;
    return 0;
}

int
userClockTimeInSeconds(const int hours, const int minutes, const int seconds) 
{
    assert((hours >= 0 && hours <= 23) && (minutes >= 0 && minutes <= 59) && (seconds >= 0 && seconds <= 59));
    return hours * 3600 + minutes * 60 + seconds;
}
